

Description
-------------
Comment Dialogue facilitates a discussion between node authors and one or more
role groups. The module sends an email notification to members of selected 
roles when a node author posts a comment to their own node and notification 
back to the node author when anyone else posts a comment on the node. Comment 
Dialogue can be enabled per node type. There are no subscription options or the 
ability to "opt out" of notification. 

It is recommended that access is restricted to just the node author and the 
role groups to view nodes and post comments where Comment Dialogue is enabled, 
for example with Content Access.


Dependencies
-------------
Comment module


Installation
-------------
1. copy the comment_dialogue directory and all its contents to your modules 
   directory
2. enable the module: admin/build/modules
3. configure the module: admin/settings/comment_dialogue
4. enable Comment Dialogue on one or more node types: admin/content/types


Download
-------------
Download package and report bugs, feature requests, or submit a patch from the 
project page on the Drupal web site.
http://drupal.org/project/comment_dialogue


Developers
-------------
The node content type form has become cluttered with all of the additional 
settings introduced. Comment-related node content type settings could be 
grouped in it's own fieldset, comment_options. This module includes a code 
block to create the fieldset if it does not already exist and move the Default 
Comment setting to this fieldset which will make the form more intuitive until 
a standard arises. Feel free to copy the code block into your own modules.


Todo List
-------------
None


Author
-------------
John Money
ossemble LLC.
http://ossemble.com

Module development sponsored by LifeWire, a subsidiary of The New York Times 
Company.
http://www.lifewire.com
